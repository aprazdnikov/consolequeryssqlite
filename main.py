# -*- coding: utf-8 -*-

import os
import sys
import sqlite3 as lite
from PyQt5 import uic
from PyQt5.QtWidgets import QApplication, QFileDialog
from PyQt5.QtCore import Qt, QAbstractTableModel, QModelIndex
from functions import DBConnect, MyCursor

_DIR_PATH = os.path.dirname(__file__)
WIDGET, BASE = uic.loadUiType(os.path.join(_DIR_PATH, 'ConsoleQuery.ui'))

_LIMIT_ROWS = 'Warning: Correct the request. the query returns more than 999,999 rows'


class ConsoleInitialization(BASE, WIDGET):

    def __init__(self):
        super(ConsoleInitialization, self).__init__(None)
        self.setupUi(self)

        self.pushButton_2.clicked.connect(lambda: self.browser())
        self.pushButton.clicked.connect(lambda: self.run_query())

        self.show()

    def browser(self):
        """
         File selection function
        """
        select_file = QFileDialog.getOpenFileName(self, 'Open file', '')[0]
        self.lineEdit.setText(select_file)

    def run_query(self):
        """
        Executing a function when the "Run query" button is pressed
        """
        if self.pushButton:
            self.notice.setText('')
            path_db = self.lineEdit.text()
            text_query = (self.plainTextEdit.toPlainText()).lower()

            try:
                with DBConnect(path_db) as db:
                    cur = db.cursor(MyCursor)
                    if (text_query.find('create table') != -1) or (text_query.find('delete') != -1):
                        self.query_create_and_delete(cur, text_query)
                    elif text_query.find('insert into') != -1 or (text_query.find('update') != -1):
                        self.query_insert_and_update(cur, text_query)
                    elif text_query.find('select') != -1:
                        headers, data = self.query_select(cur, text_query)
                        table_model = MyTableModel(self, headers, data)
                        self.tableView.setModel(table_model)
            except lite.Error as error:
                self.notice.setText('Error: ' + str(error.args[0]))
                return

    def query_create_and_delete(self, cur, text_query):
        cur.execute(text_query)
        self.notice.setText('Successfully: {0}'.format(text_query))

    def query_insert_and_update(self, cur, text_query):
        cur.execute(text_query)
        self.notice.setText('Change: {0}'.format(str(cur.lastrowid)))

    def query_select(self, cur, text_query):
        string_count = 1
        data = []
        for row in cur.execute(text_query):
            data.append(row)
            string_count += 1
            if string_count >= 1000000:
                self.notice.setText(_LIMIT_ROWS)
                break
        headers = [description[0] for description in cur.description]
        return headers, data


class MyTableModel(QAbstractTableModel):
    """
    Create QAbstractTableModel
    """
    
    # FIXME: Есть работа с курсором и выборка только части данных, сделаеть fetchmany и скользящее окно с данными.
    # Использовать методы canFetchMore, beginInsertRows, endInsertRows из класса QAbstractTableModel для того чтобы
    # взаимодействовать (уведомлять) View о том что есть ещё данные. View не будет запрашивать значения колонок для
    # строк, которые находятся за пределами области видимости и при скролле вниз запросит недостающие данные, которые
    # в этот момент и можно будет отфетчить из БД.
    # Запрос для проверки: SELECT * FROM City as c1 join City as c2 on c1.ID !=c2.ID
    
    def __init__(self, parent=None, headers=None, data=None, *args):
        QAbstractTableModel.__init__(self, parent, *args)
        self.headers = headers
        self.data = data

    def rowCount(self, parent=QModelIndex()):
        return len(self.data)

    def columnCount(self, parent=QModelIndex()):
        return len(self.headers)

    def data(self, index, role):
        if not index.isValid() or (role != Qt.DisplayRole and role != Qt.EditRole):
            return
        value = ''
        if role == Qt.DisplayRole:
            row = index.row()
            col = index.column()
            value = self.data[row][col]
        return value

    def headerData(self, section, orientation, role):
        if orientation == Qt.Horizontal and role == Qt.DisplayRole:
            return self.headers[section]
        if orientation == Qt.Vertical and role == Qt.DisplayRole:
            return "%s" % str(section + 1)
        return None


if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = ConsoleInitialization()
    sys.exit(app.exec_())
