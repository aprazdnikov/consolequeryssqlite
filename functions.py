# -*- coding: utf-8 -*-
import os
import time
import sqlite3 as lite

_DIR_PATH = os.path.dirname(__file__)


class DBConnect(object):

    """ Context Manager creates a connection to the database and closes it at the end of work.

        :param path_db: the path to the database
        :return: object "db" - connection to the database, if there is no database, connects to :memory:

    """

    __slots__ = ['path_db', 'db']

    def __init__(self, path_db=None, db=None):
        self.db = db
        self.path_db = path_db

    def __enter__(self):
        """
        Open a connection to the database
        """

        if self.path_db:
            self.db = lite.connect(self.path_db)
        else:
            self.db = lite.connect(":memory:")

        return self.db

    def __exit__(self, exc_type, exc_val, exc_tb):
        """
        Close the connection
        """
        self.db.close()


class MyCursor(lite.Cursor):

    """
    The redefined feature request. Added record of long queries to log file.
    """

    def execute(self, *args, **kwargs):
        time_start = time.clock()
        query = super(MyCursor, self).execute(*args, **kwargs)
        idle = time.clock() - time_start
        if idle >= 0.1:
            file = open(_DIR_PATH + "\sqlite_slow.log", "a+")
            file.write(*args)
            file.write("    IDLE = " + str(idle) + "\n")
            file.close()
        return query
